# FMI Customer Portal Proof of Concept

Here we test how to build an application from NPM packages.

## Getting started

- To set up the project and install relevant dependencies run: `npm install`
- After that you need to configure AWS Amplify. Run `amplify configure` and follow the instructions.

- You can then host the app locally by running: `nx serve`
