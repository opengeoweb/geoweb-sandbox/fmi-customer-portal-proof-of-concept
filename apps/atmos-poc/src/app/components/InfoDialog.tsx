/* *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Copyright 2022 - Koninklijk Nederlands Meteorologisch Instituut (KNMI)
 * Copyright 2022 - Finnish Meteorological Institute (FMI)
 * */

import * as React from 'react';

import {
  Button,
  Dialog,
  DialogTitle,
  DialogContent,
  DialogContentText,
  DialogActions,
  Box,
  Typography,
  MenuItem,
  Theme,
} from '@mui/material';

import makeStyles from '@mui/styles/makeStyles';

const useStyles = makeStyles((theme: Theme) => ({
  body: {
    color: '#00000',
  },
  link: {
    color: '#00000',
  },
}));

export const InfoDialog: React.FC = () => {
  const classes = useStyles();

  const [open, setOpen] = React.useState(false);

  const handleToggleDialog = (): void => {
    setOpen(!open);
  };

  return (
    <>
      <MenuItem onClick={handleToggleDialog} divider>
        Info
      </MenuItem>
      <Dialog
        fullWidth
        maxWidth="sm"
        open={open}
        onClose={handleToggleDialog}
        aria-labelledby="form-dialog-title"
        style={{ zIndex: 10000 }}
      >
        <DialogTitle id="form-dialog-title">GeoWeb</DialogTitle>
        <DialogContent>
          <DialogContentText>
            A demonstration on how to use GeoWeb-Core inside of your application
          </DialogContentText>
          <Box>
            <Typography variant="body1" className={classes.body}>
              The modules of the GeoWeb-Core have been imported as a dependency.
              <br />
              The following modules are used:
              <br />
              - TimeSlider
              <br />
              - MultiDimensionSelect
              <br />
              - Legend
              <br />
              - LayerManager
              <br />
              Useful links:
              <br />-
              <a
                className={classes.link}
                href="https://www.npmjs.com/package/@opengeoweb/core"
              >
                npm package
              </a>
              <br />-
              <a
                className={classes.link}
                href="https://gitlab.com/opengeoweb/opengeoweb"
              >
                Nx workspace for geoweb products
              </a>
              <br />-
              <a
                className={classes.link}
                href="https://opengeoweb.gitlab.io/opengeoweb/docs/core/"
              >
                API documentation
              </a>
            </Typography>
          </Box>
        </DialogContent>
        <DialogActions>
          <Button onClick={handleToggleDialog} color="secondary">
            Close
          </Button>
        </DialogActions>
      </Dialog>
    </>
  );
};
