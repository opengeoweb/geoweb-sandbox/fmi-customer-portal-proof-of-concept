import * as React from 'react';
import makeStyles from '@mui/styles/makeStyles';
import {
  store,
  ConfigurableConnectedMap,
  CoreThemeStoreProvider,
  LegendConnect,
  LayerManagerConnect,
  ZoomControlConnect,
  generateMapId,
} from '@opengeoweb/core';
import { ReduxLayer } from '@opengeoweb/core/lib/store/mapStore/types';

export const ConfigurableConnectedMapDemo = ({
  layer,
}: {
  layer: ReduxLayer;
}): React.ReactElement => {
  const classes = useStyles();
  const mapId = generateMapId();

  return (
    <CoreThemeStoreProvider store={store}>
      <div className={classes.container}>
        <LegendConnect />
        <LayerManagerConnect />
        <div className={classes.zoomControls}>
          <ZoomControlConnect mapId={mapId} />
        </div>
        <ConfigurableConnectedMap
          id={mapId}
          layers={[
            {
              ...layer,
              style: 'precip-blue-transparent/nearest',
            },
          ]}
          shouldShowZoomControls
          displayTimeInMap
          shouldAutoUpdate
          shouldAnimate
        />
      </div>
    </CoreThemeStoreProvider>
  );
};

//Hardcoded layers. Not in use now.
const radarLayer = {
  service: 'https://openwms.fmi.fi/geoserver/wms?',
  name: 'Radar:suomi_dbz_eureffin',
  format: 'image/png',
  enabled: true,
  style: 'Radar dbz Summer',
  id: 'test',
  layerType: 'mapLayer',
} as ReduxLayer;

const humidityLayer = {
  enabled: true,
  format: 'image/png',
  id: 'relative_humidity__at_pl',
  layerType: 'mapLayer',
  name: 'relative_humidity__at_pl',
  service: 'https://geoservices.knmi.nl/adagucserver?dataset=HARM_N25&',
  style: '',
} as ReduxLayer;

const useStyles = makeStyles(() => ({
  container: {
    height: '100%',
  },
  zoomControls: {
    position: 'absolute',
    top: 24,
  },
}));

export default ConfigurableConnectedMapDemo;
