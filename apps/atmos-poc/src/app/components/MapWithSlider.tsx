import React, { useEffect } from 'react';
import { useDispatch, Provider } from 'react-redux';

import {
  mapActions,
  MapViewConnect,
  LegendConnect,
  LegendMapButtonConnect,
  TimeSliderConnect,
  CoreThemeStoreProvider,
  store,
  LayerManagerConnect,
  ZoomControlConnect,
} from '@opengeoweb/core';
import { ReduxLayer } from '@opengeoweb/core/lib/store/mapStore/types';
import makeStyles from '@mui/styles/makeStyles';

const baseLayerGrey = {
  // "id": "base-layer-1",
  id: 'map-1',
  name: 'WorldMap_Light_Grey_Canvas',
  type: 'twms',
  layerType: 'baseLayer',
} as ReduxLayer;

const overLayer = {
  id: 'map-2',
  service: 'https://geoservices.knmi.nl/wms?DATASET=baselayers&',
  name: 'countryborders',
  format: 'image/png',
  enabled: true,
  layerType: 'overLayer',
} as ReduxLayer;

const radarLayer = {
  service: 'https://openwms.fmi.fi/geoserver/wms?',
  name: 'Radar:suomi_dbz_eureffin',
  format: 'image/png',
  enabled: true,
  style: 'Radar dbz Summer',
  id: 'test',
  layerType: 'mapLayer',
} as ReduxLayer;

const ConnectedMapWithTimeSlider = ({ mapId }: { mapId: string }) => {
  const dispatch = useDispatch();
  const classes = useStyles();
  //const mapId = 'map-1';

  useEffect(() => {
    // set layers
    dispatch(mapActions.setLayers({ layers: [radarLayer], mapId }));
    // baseLayers
    dispatch(
      mapActions.setBaseLayers({
        mapId,
        layers: [baseLayerGrey, overLayer],
      })
    );
  }, []);

  return (
    <CoreThemeStoreProvider store={store}>
      <div className={classes.container}>
        <LegendConnect showMapId />
        <LayerManagerConnect />
        <div className={classes.zoomControls}>
          <ZoomControlConnect mapId={mapId} />
        </div>
        <LegendConnect showMapId />
        <LegendMapButtonConnect mapId={mapId} />
        <MapViewConnect mapId={mapId} displayTimeInMap />
        <TimeSliderConnect sourceId="timeslider-1" mapId={mapId} />
      </div>
    </CoreThemeStoreProvider>
  );
};

const map = ({ mapId }: { mapId: string }) => (
  <Provider store={store}>
    <ConnectedMapWithTimeSlider mapId={mapId} />
  </Provider>
);

const useStyles = makeStyles(() => ({
  container: {
    height: '100vh',
  },
  zoomControls: {
    position: 'absolute',
    top: 24,
  },
}));

// export default ConnectedMapWithTimeSlider;
export default map;
