import React from "react";
import ReactGridLayout, { WidthProvider, Responsive } from "react-grid-layout";
import makeStyles from '@mui/styles/makeStyles';
import _ from "lodash";
import MapDemo from './configurableMap';
import MultiMap from './MultiMap';
import '/node_modules/react-grid-layout/css/styles.css';
import '/node_modules/react-resizable/css/styles.css';
import { ReduxLayer } from "@opengeoweb/core/lib/store/mapStore/types";

const ResponsiveReactGridLayout = WidthProvider(Responsive);


interface DashBoardProps {
  layers: ReduxLayer[]
}

export const DashBoard: React.FC<DashBoardProps> = ({
  layers
}) => {
  const classes = useStyles();
  const cols = { lg: 16, md: 12, sm: 8, xs: 4, xxs: 4};
  const rowHeight = 100;

  const layoutBase = {
    w: 4,
    h: 4,
    minW: 2,
    minH: 2,
    maxH: 4,
    maxW: 4,
  }

  const [counter, setCounter] = React.useState(layers.length);
  const [editable, setEditable] = React.useState<boolean>(false);
  const [overlap, setOverlap] = React.useState<boolean>(false);
  const [mapViewType, setMapViewType] = React.useState<number[]>([]);
  const [layout, setLayout] = React.useState<ReactGridLayout.Layouts>({});
  const [items, setItems] = React.useState<ReactGridLayout.Layout[]> ([...Array(layers.length).keys()].map((i) => {
    return {
      ...layoutBase,
      i: i.toString(),
      x: i * 4,
      y: 0,
    }
  }));

  const createElement = (el: any) => {
    const i = el.add ? "+" : el.i;

    return (
      <div key={i} data-grid={el} className={classes.box}>
        {mapViewType.includes(Number(i))
          ? <MultiMap />
          // : <MultiMap />
          : <MapDemo layer={layers[i]}/>
        }
        <span
          className={classes.mapToggle}
          onClick={() => onMapTypeToggle(i)}
        >
          t
        </span>
        {/* <span
          className={classes.fullScreen}
          onClick={() => onFullScreenItem(i)}
        >
          f
        </span> */}
        <span
          className={classes.remove}
          onClick={() => onRemoveItem(i)}
        >
          x
        </span>
      </div>
    );
  }

  const onAddItem = () => {
    setCounter(counter + 1);
    setItems(items.concat({
      ...layoutBase,
      i: counter.toString(),
      x: (items.length * 4),
      y: 0,
    }))
  }

  const onBreakpointChange = (breakpoint: any, cols: any) => {
    console.log({breakpoint, cols})
    // this.setState({
    //   breakpoint: breakpoint,
    //   cols: cols
    // });
  }

  const onLayoutChange = (layout: ReactGridLayout.Layout[], allLayouts: ReactGridLayout.Layouts) => {
    setLayout(allLayouts);
  }

  const onResize: ReactGridLayout.ItemCallback = (layout, oldItem, newItem, placeHolder) => {
    let w = Math.round(newItem.w / 4) * 4;
    w = w <= layoutBase.maxW ? w : layoutBase.maxW;
    placeHolder.w = w;
    newItem.w = w;
    window.dispatchEvent(new Event('resize'));
  }

  const onResizeStop = () => {
    window.dispatchEvent(new Event('resize'));
  }

  const onDrag: ReactGridLayout.ItemCallback = (layout, oldItem, newItem, placeHolder) => {
    const x = Math.round(newItem.x / 4) * 4;
    placeHolder.x = x;
  }

  const onDragStop: ReactGridLayout.ItemCallback = (layout, oldItem, newItem, placeHolder) => {
    const x = Math.round(newItem.x / 4) * 4;
    newItem.x = x;

  }

  const onRemoveItem = (i: number) => {
    setItems((_.reject(items, { i: i })) as ReactGridLayout.Layout[]);
  }

  // const onFullScreenItem = (i: number) => {
    // console.log(layout);
    // const newLayout: ReactGridLayout.Layouts = {};
    // Object.keys(layout).forEach((key) => {
    //   newLayout[key] = layout[key].map((item) => {
    //     const [w,h] = item.i === String(i) ? [8,8]: [0,0];
    //     return {...items, w ,h};
    //   })
    // });
    // // console.log(newLayout);
    // setOverlap(true);
    // setLayout(newLayout);
    // setLayout(layout.map((item) => {
    //   const [x,y] = item?.i === String(i) ? [8,8] : [0,0];
    //   return {...item, x, y};
    // }
    // ))
    // console.log(items[i]);
  // }

  const onMapTypeToggle = (i: number) => {
    setMapViewType(mapViewType.includes(Number(i)) ? mapViewType.filter((v) => v !== Number(i)) : [...mapViewType, Number(i)])
  }

  return (
    <div>
      <button onClick={onAddItem}>Add Item</button>
      <button onClick={() => setEditable(!editable)}>Edit Items</button>
      <button onClick={() => setOverlap(!overlap)}>Overlap Items</button>
      <div className={classes.container}>
        <ResponsiveReactGridLayout
          isBounded
          isResizable={editable}
          isDraggable={editable}
          allowOverlap={overlap}
          onResize={onResize}
          onResizeStop={onResizeStop}
          onDrag={onDrag}
          onDragStop={onDragStop}
          onLayoutChange={onLayoutChange}
          layouts={layout}
          onBreakpointChange={onBreakpointChange}
          className={classes.layout}
          cols={cols}
          rowHeight={rowHeight}
        >
          {_.map(items, el => createElement(el))}
        </ResponsiveReactGridLayout>
      </div>
    </div>
  );

}

const useStyles = makeStyles(() => ({
  container: {
    width: '100%',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
  },
  layout: {
    width: '100%',
    border: '1px solid black',
  },
  box: {
    border: '1px solid black',
    padding: '10px',
  },
  remove: {
    position: "absolute",
    right: "5px",
    zIndex: 99999,
    top: 0,
    cursor: "pointer"
  },
  fullScreen: {
    position: "absolute",
    right: "25px",
    zIndex: 99999,
    top: 0,
    cursor: "pointer"
  },
  mapToggle: {
    position: "absolute",
    right: "45px",
    zIndex: 99999,
    top: 0,
    cursor: "pointer"
  }
}));



export default DashBoard;