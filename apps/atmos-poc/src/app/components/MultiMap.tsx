import * as React from 'react';
import {
  store,
  CoreThemeStoreProvider,
  LayerManagerConnect,
  LegendConnect,
  MultiMapDimensionSelectConnect,
  MultiMapPreset,
  MultiMapViewConnect,
} from '@opengeoweb/core';
import { ReduxLayer } from '@opengeoweb/core/lib/store/mapStore/types';

export const MultiMapViewStory2x2 = (): React.ReactElement => {
  const maps: MultiMapPreset[] = [
    {
      id: 'precipradarnl',
      title: 'Precipitation Radar NL',
      layers: [radarLayer],
    },
    {
      id: 'temperatureobs',
      title: 'Temperature Observations',
      layers: [radarLayer],
    },
    {
      id: 'harmonieprecip',
      title: 'Harmonie precipitation',
      layers: [radarLayer],
    },
    {
      id: 'msgnaturalview',
      title: 'EumetSat MSG natural view',
      layers: [radarLayer],
    },
  ];

  return (
    <CoreThemeStoreProvider store={store}>
      <div style={{ width: '100%', height: '100%' }}>
        <MultiMapViewConnect rows={2} cols={2} maps={maps} />
        <LegendConnect showMapId />
        <MultiMapDimensionSelectConnect />
        <LayerManagerConnect showTitle />
      </div>
    </CoreThemeStoreProvider>
  );
};

const radarLayer = {
  service: 'https://openwms.fmi.fi/geoserver/wms?',
  name: 'Radar:suomi_dbz_eureffin',
  format: 'image/png',
  enabled: true,
  style: 'Radar dbz Summer',
  id: 'test',
  layerType: 'mapLayer',
} as ReduxLayer;

export default MultiMapViewStory2x2;
