export const myAppConfig = {
  // ...
  aws_appsync_graphqlEndpoint:
    'https://loogfyapxbcsdme3jqw7vzf27q.appsync-api.eu-north-1.amazonaws.com/graphql',
  aws_appsync_region: 'eu-north-1',
  aws_appsync_authenticationType: 'AMAZON_COGNITO_USER_POOLS', // You have configured Auth with Amazon Cognito User Pool ID and Web Client Id
  // ...
};
