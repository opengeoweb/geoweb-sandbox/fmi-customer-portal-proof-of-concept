import { ReduxLayer } from '@opengeoweb/core/lib/store/mapStore/layers/types';
import { API } from 'aws-amplify';
import React from 'react';
import { getUserPresets } from '../../graphQL/queries/presets';
import MapDemo from '../components/configurableMap';
import DashBoard from '../components/dashBoard';

declare type awsPreset = {
  data: { getUserPresets: { presets: ReduxLayer[] } };
};

const WrapperHelper: React.FC = () => {
  const [layers, setLayers] = React.useState<ReduxLayer[]>([]);

  const getAnswer = async () => {
    const res = (await API.graphql({
      query: getUserPresets,
    })) as Promise<awsPreset>;
    const data = (await res).data.getUserPresets.presets;
    setLayers(data);
  };

  React.useEffect(() => {
    getAnswer();
  }, []);

  return (
    <>
      <div style={{ height: '100vh' }}>
        {layers.length === 1 && <MapDemo layer={layers[0]} />}
        {layers.length > 1 && <DashBoard layers={layers} />}
      </div>
    </>
  );
};

export default WrapperHelper;
