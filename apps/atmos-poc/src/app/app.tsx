// eslint-disable-next-line @typescript-eslint/no-unused-vars
import MapDemo from './utils/wrapperFunction';
import { Authenticator } from '@aws-amplify/ui-react';
import '@aws-amplify/ui-react/styles.css';

function App() {
  return (
    <Authenticator>
      {({ signOut }) => (
        <div>
          <button onClick={signOut}>Sign out</button>
          <MapDemo />
        </div>
      )}
    </Authenticator>
  );
}

export default App;
