export const getUserPresets = `
query getUserPresets {
    getUserPresets {
      presets {
        enabled
        format
        id
        layerType
        name
        service
        style
      }
    }
  }`;
