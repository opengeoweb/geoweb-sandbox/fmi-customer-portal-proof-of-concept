import 'babel-polyfill';
import React from 'react';
import ReactDOM from 'react-dom';
import App from './app/app';
import { Amplify } from 'aws-amplify';
import { myAppConfig } from './app/utils/awsAppsync';
import { cognitoAuth } from './app/utils/awsCognito';
import awsExports from '../../../src/aws-exports';
Amplify.configure(awsExports);
Amplify.configure(myAppConfig);
Amplify.configure(cognitoAuth);

ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
